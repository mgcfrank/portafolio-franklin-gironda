import { createApp } from "vue";
import App from "./App.vue";
import global_ from './utils/global'
import router from "./router";


const app = createApp(App);
app.config.globalProperties.GLOBAL = global_
app.use(router);
app.mount("#app");

// createApp(App).use(router).mount("#app");
