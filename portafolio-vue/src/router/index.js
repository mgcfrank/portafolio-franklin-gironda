import { createRouter, createWebHashHistory } from 'vue-router';
import GLOBAL from '@/utils/global.js';

const routes = [
   {
      path: '/',
      name: 'Inicio',
      component: () => import('../views/inicio/index.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/skills',
      name: 'Skills',
      component: () => import('../views/skills/index.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/habilidades',
      name: 'Habilidades',
      component: () => import('../views/habilidades/index.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/habilidades/backend',
      name: 'Backend',
      component: () => import('../views/habilidades/backend.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/habilidades/frontend',
      name: 'frontend',
      component: () => import('../views/habilidades/frontend.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/habilidades/diseno',
      name: 'diseno',
      component: () => import('../views/habilidades/diseno.vue'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/portafolio',
      name: 'Portafolio',
      component: () => import('../views/portafolio'),
      meta: { authRequired: 'false' },
   },
   {
      path: '/portafolio/:id',
      name: 'Portafolio detalle',
      component: () => import('../views/portafolio/detalle.vue'),
      meta: { authRequired: 'true' },
   },
   {
      path: '/contactos',
      name: 'Contactos',
      component: () => import('../views/contactos'),
      meta: { authRequired: 'false' },
   },
   // {
   //    path: '/usuarios',
   //    name: 'Usuarios',
   //    component: () => import('../views/usuarios'),
   //    meta: { authRequired: 'true' },
   //    icon: 'fas fa-users',
   // },
   // {
   //    path: '/servicios',
   //    name: 'Servicios',
   //    component: () => import('../views/servicios'),
   //    meta: { authRequired: 'true' },
   //    icon: 'fas fa-store',
   // },
   // {
   //    path: '/mascotas',
   //    name: 'Mascotas',
   //    component: () => import('../views/mascotas'),
   //    meta: { authRequired: 'true' },
   //    icon: 'fas fa-paw',
   // },
   // {
   //    path: '/albergues',
   //    name: 'Albergues',
   //    component: () => import('../views/albergues'),
   //    meta: { authRequired: 'true' },
   //    icon: 'fas fa-warehouse',
   // },
   // {
   //    path: '/publicidad',
   //    name: 'Publicidad',
   //    component: () => import('../views/publicidad'),
   //    meta: { authRequired: 'true' },
   //    icon: 'far fa-newspaper',
   // },
   // //OCULTOS:::::::::::::::::::
   // {
   //    path: '/albergue-mascotas/:id_albergue',
   //    name: 'Albergue mascotas',
   //    component: () => import('../views/albergues/MascotasAlbergue'),
   //    meta: { authRequired: 'true' },
   // },
   // {
   //    path: '/mis-mascotas/:id_usuario',
   //    name: 'Mis mascotas',
   //    component: () => import('../views/usuarios/MisMascotas.vue'),
   //    meta: { authRequired: 'true' },
   // },
   // {
   //    path: '/home',
   //    name: 'Home',
   //    component: Home,
   // },
   // //login
   // {
   //    path: '/autentifica',
   //    name: 'autentifica',
   //    component: () => import('../views/login'),
   //    meta: { authRequired: 'false' },
   // },
   //paginas extras
   {
      //pagina no autorizada
      path: '/not-authorized',
      name: 'not-authorized',
      props: true,
      component: () => import('../views/000/NotAuthorized.vue'),
      meta: { authRequired: 'false' },
   },
   {
      //pagina no autorizada
      path: '/nofound',
      name: 'nofound',
      props: true,
      component: () => import('../views/000/404.vue'),
      meta: { authRequired: 'false' },
   },
   {
      //pagina no encontrada
      path: '/:pathMatch(.*)',
      component: () => import('../views/000/404.vue'),
      meta: { authRequired: 'false' },
   },
];

const router = createRouter({
   history: createWebHashHistory(),
   // history: createWebHistory(),
   routes,
});

// router.beforeEach((to, from, next) => {
//    // preguntamos si la ruta existe
//    if (to.name === undefined) {
//       next({ name: 'nofound' });
//    } else {
//       let estado = to.meta.authRequired;
//       // preguntamos si la pagina requiere autenticacion
//       if (estado === 'true') {
//          GLOBAL.log('Autentificacion ' + estado);
//          // preguntamos si existe una sesion
//          if (localStorage.getItem('id_usuario')) {
//             //preguntamos si pide rol la pagina
//             if (to.meta.rolAcces) {
//                //preguntamos si el usuario tiene acceso
//                if (
//                   localStorage.getItem('role') == to.meta.rolAcces[0] ||
//                   localStorage.getItem('role') == to.meta.rolAcces[1]
//                ) {
//                   //procedemos a mostrar la pagina deseada
//                   next();
//                   console.log('Muestra pagina');
//                } else {
//                   //redirige al inicio
//                   next({ name: 'inicio' });
//                   console.log('Muestra inicio');
//                }
//             } else {
//                //muestra la pagina deseada
//                next();
//                console.log('Muestra pagina');
//             }
//          } else {
//             next({ name: 'autentifica' });
//             console.log('Muestra login');
//          }
//       } else {
//          //Preguntamso si esta autentificado y si la pagina es login
//          if (to.name === 'autentifica' && localStorage.getItem('id_usuario')) {
//             next({ name: 'inicio' });
//             console.log('Muestra inicio');
//          } else {
//             //procedemos a mostrar la pagina
//             next();
//             console.log('Muestra pagina no auth');
//          }
//       }
//    }
// });

export default router;
