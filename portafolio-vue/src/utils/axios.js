// import Vue from 'vue'
import axios from 'axios';
import GLOBAL from '@/utils/global.js';

//configuramos la url base del api
const baseURL = GLOBAL.baseUrlApi();
// const baseURL =
//    process.env.VUE_APP_NODE_ENV == 'local'
//       ? process.env.VUE_APP_BASE_API_LOCAL
//       : process.env.VUE_APP_BASE_API_PRODUCTION;

let apiJson = axios.create({
   baseURL: baseURL,
   // headers: {
   //    Authorization: 'Bearer ' + localStorage.getItem('access_token'),
   //    'Access-Control-Allow-Origin': '*',
   //    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
   // },
});
// response interceptor
apiJson.interceptors.response.use(
   (response) => {
      //envia respuestas recibidas
      GLOBAL.log(response);
      return response;
   },
   (err) => {
      GLOBAL.log(err);
      if (err.response) {
         //verificamos que el token no haya caducado
         if (err.response.status == 401) {
         }
      } else if (err.request) {
         // client never received a response, or request never left
         alert('No hay respuesta del servidor: \n\n' + err.request);
      } else {
         // anything else
         alert('No hay conexion con el servidor: \n\n' + err);
      }
      //envia respuestas fallidas 500 o 400
      return Promise.reject(err);
   }
);

function sendForm(method, url, dataForm = {}) {
   return apiJson({
      url: url,
      method: method,
      data: dataForm,
      headers: {
         // 'Content-Type': 'application/x-www-form-urlencoded',
         'Content-Type': 'multipart/form-data',
      },
   });
}

function send(method, url, dataForm = {}) {
   return apiJson({
      url: url,
      method: method,
      data: dataForm,
      headers: {
         'Content-Type': 'application/json',
      },
   });
}

export default {
   refresh() {
      apiJson = axios.create({
         baseURL: baseURL,
         headers: {
            Authorization: 'Bearer ' + localStorage.getItem('access_token'),
         },
      });
   },
   //AUTH
   login({ form }) {
      return sendForm('POST', '/usuarios/acceso', form);
   },

   //USUARIO
   userVerifyNumber({ celular }) {
      return send('GET', '/usuario/numero-verifica?celular=' + celular);
   },

   getUsuarios({ buscar = '', cantidad_por_pagina = 20, pagina = 1 }) {
      return send(
         'GET',
         '/usuarios?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina
      );
   },
   createUsuarios({ form }) {
      return sendForm('POST', '/usuarios/registra', form);
   },
   updateUsuarios({ form }) {
      return sendForm('POST', '/usuarios/actualizar', form);
   },

   // END POINT MIS MASCOTAS
   getMiMascota({ buscar = '', cantidad_por_pagina = 20, pagina = 1, id_usuario }) {
      return send(
         'GET',
         '/mis-mascotas?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina +
            '&id_usuario=' +
            id_usuario
      );
   },
   createMiMascota({ form }) {
      return sendForm('POST', '/mis-mascotas/registra', form);
   },
   updateMiMascota({ form }) {
      return sendForm('POST', '/mis-mascotas/actualizar', form);
   },
   deleteMiMascota({ id }) {
      return send('DELETE', '/mis-mascotas/eliminar/' + id);
   },

   //END POINTS SERVICIOS
   getServicios({ buscar = '', cantidad_por_pagina = 20, pagina = 1 }) {
      return send(
         'GET',
         '/servicios?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina
      );
   },
   createServicios({ form }) {
      return sendForm('POST', '/servicios/registra', form);
   },
   updateServicios({ form }) {
      return sendForm('POST', '/servicios/actualizar', form);
   },
   deleteServicios({ id }) {
      return send('DELETE', '/servicios/eliminar/' + id);
   },
   getServiciosGaleria({ id_servicio }) {
      return send('GET', '/servicios/galeria?id_servicio=' + id_servicio);
   },
   createServiciosGaleria({ form }) {
      return sendForm('POST', '/servicios/galeria/registra', form);
   },
   updateServiciosGaleria({ form }) {
      return sendForm('POST', '/servicios/galeria/actualizar', form);
   },
   deleteServiciosGaleria({ id }) {
      return send('DELETE', '/servicios/galeria/eliminar/' + id);
   },
   //END POINTS MASCOTAS
   getMascota({ buscar = '', cantidad_por_pagina = 20, pagina = 1 }) {
      return send(
         'GET',
         '/mascotas?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina
      );
   },
   createMascota({ form }) {
      return sendForm('POST', '/mascotas/registra', form);
   },
   updateMascota({ form }) {
      return sendForm('POST', '/mascotas/actualizar', form);
   },
   updateMascotaEstado({ form }) {
      return sendForm('POST', '/mascotas/actualizar/estado', form);
   },
   deleteMascota({ id }) {
      return send('DELETE', '/mascotas/eliminar/' + id);
   },
   getMascotaGaleria({ id_mascota }) {
      return send('GET', '/mascotas/galeria?id_mascota=' + id_mascota);
   },
   createMascotaGaleria({ form }) {
      return sendForm('POST', '/mascotas/galeria/registra', form);
   },
   updateMascotaGaleria({ form }) {
      return sendForm('POST', '/mascotas/galeria/actualizar', form);
   },
   deleteMascotaGaleria({ id }) {
      return send('DELETE', '/mascotas/galeria/eliminar/' + id);
   },

   // END POINT ALBERGUES
   getAlbergue({ buscar = '', cantidad_por_pagina = 20, pagina = 1 }) {
      return send(
         'GET',
         '/albergues?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina
      );
   },
   createAlbergue({ form }) {
      return sendForm('POST', '/albergues/registra', form);
   },
   updateAlbergue({ form }) {
      return sendForm('POST', '/albergues/actualizar', form);
   },
   deleteAlbergue({ id }) {
      return send('DELETE', '/albergues/eliminar/' + id);
   },
   // END POINT MASCOTAS - ALBERGUES
   getAlbergueMascota({ buscar = '', cantidad_por_pagina = 20, pagina = 1, id_albergue }) {
      return send(
         'GET',
         '/albergue-mascota?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina +
            '&id_albergue=' +
            id_albergue
      );
   },
   createAlbergueMascota({ form }) {
      return sendForm('POST', '/albergue-mascota/registra', form);
   },
   updateAlbergueMascota({ form }) {
      return sendForm('POST', '/albergue-mascota/actualizar', form);
   },
   deleteAlbergueMascota({ id }) {
      return send('DELETE', '/albergue-mascota/eliminar/' + id);
   },
   getAlbergueMascotaGaleria({ id_mascota_albergue }) {
      return send('GET', '/albergue-mascota/galeria?id_mascota_albergue=' + id_mascota_albergue);
   },
   createAlbergueMascotaGaleria({ form }) {
      return sendForm('POST', '/albergue-mascota/galeria/registra', form);
   },
   updateAlbergueMascotaGaleria({ form }) {
      return sendForm('POST', '/albergue-mascota/galeria/actualizar', form);
   },
   deleteAlbergueMascotaGaleria({ id }) {
      return send('DELETE', '/albergue-mascota/galeria/eliminar/' + id);
   },

   // END POINT PUBLICIDAD
   getMonetiza({ buscar = '', cantidad_por_pagina = 20, pagina = 1, estado=''}) {
      return send(
         'GET',
         '/monetiza?buscar=' +
            buscar +
            '&cantidad_por_pagina=' +
            cantidad_por_pagina +
            '&pagina=' +
            pagina+'&estado='+estado
      );
   },
   createMonetiza({ form }) {
      return sendForm('POST', '/monetiza/registra', form);
   },
   updateMonetiza({ form }) {
      return sendForm('POST', '/monetiza/actualizar', form);
   },
   deleteMonetiza({ id }) {
      return send('DELETE', '/monetiza/eliminar/' + id);
   },
};
// Vue.prototype.$axios = axios
