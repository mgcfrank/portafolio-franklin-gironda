export default {
   isThereALogin(){
      return 
   },
   baseUrlApi() {
      return process.env.VUE_APP_NODE_ENV == 'local'
         ? process.env.VUE_APP_BASE_API_LOCAL
         : process.env.VUE_APP_BASE_API_PRODUCTION;
   },
   baseUrl(path = '') {
      return process.env.VUE_APP_NODE_ENV == 'local'
         ? process.env.VUE_APP_BASE_URL_LOCAL + path
         : process.env.VUE_APP_BASE_URL_PRODUCTION + path;
   },
   log(text) {
      if (process.env.VUE_APP_NODE_ENV == 'local') {
         console.log(text);
      }
   },
   generateUUID() {
      var dt = new Date().getTime();
      var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
        /[xy]/g,
        function (c) {
          var r = (dt + Math.random() * 16) % 16 | 0;
          dt = Math.floor(dt / 16);
          return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
        }
      );
      return uuid;
    },
    isItAnEmail(valor) {
      if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
       return true;
      } else {
       return false
      }
    },
    isItANumber(valor){
      if (valor.match(/^[0-9]+$/)){
         return true;
        } else {
         return false
        }
    },
    isItANumberDecimal(valor){
       let valoresAceptados=/^[0-9]+$/;
      if (valor.indexOf(".") === -1 ){
         if (valor.match(valoresAceptados)){
            return true;
           } else {
            return false
           }
      }else{
         //dividir la expresión por el punto en un array
         var particion = dato.split(".");
         //evaluamos la primera parte de la división (parte entera)
         if (particion[0].match(valoresAceptados) || particion[0]==""){
             if (particion[1].match(valoresAceptados)){
                 return true;
             }else {
                 return false;
             }
         }else{
             return false;
         }
      }
    },

    roundNumber(numb, decimal){
      return numb.toFixed(decimal);
    },
    textNoEmpty(value){
       return value==null || value=="null" || value.length<=0? '---':value;
    }

};
